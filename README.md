# `log4j`

[Log4j](https://logging.apache.org/log4j/2.x/) is one of the most popular Java logging frameworks.

This repository provides a basic binary as an example of how to use the `log4j2` framework with Bazel.

## Getting Started

Assuming that you have [Bazel](https://bazel.build/) installed, you can run the example with

```
bazel run //java/logging:Example
```

This will build and run the Docker image `bazel/java/logging:Example` locally:

```
2020-10-20 08:36:31,474 [main] Log4j with Bazel!
```
