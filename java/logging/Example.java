package logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Example {
  private static final Logger logger = LogManager.getLogger(Example.class);

  public static void main(String[] args) {
    logger.info("Log4j with Bazel!");
  }
}
